var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var extend = require('extend');
var parseArgs = require('minimist');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var pug = require('gulp-pug');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var browserSync = require('browser-sync').create();
var wiredep = require('wiredep').stream;
var mainBowerFiles = require('main-bower-files');
var removeHtmlComments = require('gulp-remove-html-comments');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var gulpif = require('gulp-if');
var data = require('gulp-data');
var path = require('path');
var requireGlob = require('require-glob');
var plumber = require('gulp-plumber');

var config = extend({
  env: process.env.NODE_ENV
}, parseArgs(process.argv.slice(2)));

// BrowserSync task to serve dist folder
gulp.task('bs-serve', function(done){
  browserSync.init({
      server: {
        baseDir: './dist',
        routes: {
          '/bower_components': 'bower_components'
        }
      },
      open: false
  });
  done();
});


//reloads the browser
function reload(done){
  browserSync.reload();
  done();
}


// Clean dist folder
gulp.task('clean', function () {
  return del('dist');
});


// Compile sass into CSS
gulp.task('styles', styles);
function styles() {
  return gulp.src('src/styles/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browserSync.stream());
}


// Compile PUG into HTML
// and inject sources
gulp.task('views', views);
function views(){

  var srcScripts = gulp.src([
    'src/scripts/utils/**/*.js',
    'src/scripts/config.js',
    'src/scripts/autoload/**/*.js',
    'src/scripts/**/*.js'
  ]);

  var vendorScripts = gulp.src(mainBowerFiles({filter: '**/*.js'}));
  var vendorStyles = gulp.src(mainBowerFiles({filter: '**/*.css'}));

  return gulp.src(['src/views/*.pug', 'src/views/pages/**/*.pug', '!src/views/**/_*.pug'])
    .pipe(plumber())
    .pipe(data(file => {
      var data = requireGlob('src/data/**/*.json', { bustCache: true })
      return data;
    }))
    .pipe(pug({
      pretty: true,
    }))
    .on('error', onError)
    .pipe(inject(vendorStyles.pipe(concat('vendor.css')).pipe(gulp.dest('./dist/assets/vendor')), { name: 'vendor', ignorePath: 'dist', addRootSlash: false}))
    .pipe(inject(vendorScripts.pipe(concat('vendor.js'))
      .pipe( gulpif(config.env === 'production', uglify() ))
      .pipe(gulp.dest('dist/assets/vendor')), { name: 'vendor', ignorePath: 'dist', addRootSlash: false}))

    .pipe(inject(srcScripts.pipe(sourcemaps.init()).pipe(concat('main.js'))
      .pipe(babel({
        presets: ['env']
      }))
      .pipe( gulpif(config.env === 'production', uglify() ))
      .pipe(sourcemaps.write(''))
      .pipe(gulp.dest('dist/assets/js')), { ignorePath: 'dist', addRootSlash: false}))

    .pipe( gulpif(config.env === 'production', removeHtmlComments()))
    .pipe(gulp.dest('dist'));
};


function onError(err) {
  console.log(err);
  this.emit('end');
}

// Copy assets folder to dist
gulp.task('assets', assets);
function assets() {
  return gulp.src('src/assets/**/*.*')
    .pipe(gulp.dest('dist/assets'))
};

gulp.task('env:dev', setDevEnv);
function setDevEnv(done) {
  process.env.NODE_ENV = config.env = 'development';
  console.log('Environment is now:', process.env.NODE_ENV)
  done();
};

gulp.task('env:prod', setProdEnv);
function setProdEnv(done) {
  process.env.NODE_ENV = config.env = 'production';
  console.log('Environment is now:', process.env.NODE_ENV)
  done();
};

// Watchers
gulp.task('watch', watch);
function watch() {
  gulp.watch('src/styles/**/*.scss', styles);
  gulp.watch(['src/**/*.pug', 'src/scripts/**/*.js', 'src/data/**/*.json'], gulp.series('views', reload));
  gulp.watch(['src/assets/**/*.*'], gulp.series('assets', reload));
}


// series
// gulp build task
gulp.task('build', gulp.series('env:prod', 'clean', 'assets', 'styles', 'views'))


// gulp serve task
gulp.task('serve', gulp.series('env:dev', 'clean', 'assets', 'styles', 'views', 'bs-serve', 'watch'));
