/**
 * load order:
 *  1. utils / ** / *.js
 *  2. config.js
 *  3. autload / ** / *.js
 *  4. ** / *.js
 *
 */


const STAGING = ENV == 'development' ? true : false;
const HANDOVER = {
  data: './assets/data.json'
}

var OPTIONS = {
  loading: true,
}

/**
 *
 * Function to get data
 * for the first time
 *
 * handles with page loader
 *
 */
var DATA = {};
var dataLoaded = false;
var initing = false;
var isPage = AppUtils.isPage;

var init = (cb) => {
  if(!initing){
    initing = true;
    if(dataLoaded){
      if ($.isFunction(cb)) {
        initing = false;
        return cb(DATA)
      }
    } else {
      AppUtils.getData(HANDOVER.data, (data) => {
        DATA = data;
        dataLoaded = true;
        initing = false;
      })
    }
  }
  setTimeout(function () {
    init(cb);
  }, 10);
}