var AppUtils = {
  getData: function(url, cb){
    $.get(url)
      .done(function (res) {
        if (STAGING) {
          console.log('[STAGING].[AppUtils.getData] -> res: ', res);
        }
        if ($.isFunction(cb)) {
          return cb(res);
        }
      }).fail(function (err) {
        console.log('Error: ', err);
        if ($.isFunction(cb)) {
          return cb();
        }
      })
  },

  toggleLoading: function(){
    var loader = $('*[data-loading]');
    if(loader.hasClass('active')){
      setTimeout(function () {
        loader.removeClass('active');
        $('html, boddy').css({'overflow-y': ''});
      }, 3000);
    }else{
      loader.addClass('active');
    }
  },

  isPage: function(page, cb){
    var className = " " + page + " ";
    if ( (" " + document.body.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + page + " ") > -1 ){
      return cb(true) || true;
    } else {
      return false;
    }
  },


  getUrlData: function() {
    return new Promise(function (resolve, reject) {

      data = [];

      var urlData = window.location.search.substring(1);
      if (!urlData) {
        reject('no data!');
      }
      // uncoment this if the URI have + instead of spaces
      // urlData = urlData.replace(/\+/g, '%20')

      var fields = urlData.split('&');

      for (var i = 0; i < fields.length; i++) {
        var field = (fields[i].split('='));
        var obj = {
          name: field[0],
          val: decodeURIComponent(field[1])
        }
        data.push(obj);

        if (i + 1 === fields.length) {

          if (data.length) {
            resolve(data);
          } else {
            reject('no data found!');
          }
        }
      }
    });
  },

  getUrlObject: function() {
    return new Promise(function (resolve, reject) {

      data = {};

      var urlData = window.location.search.substring(1);
      if (!urlData) {
        reject('no data!');
      }

      var fields = urlData.split('&');

      for (var i = 0; i < fields.length; i++) {
        var field = (fields[i].split('='));
        data[field[0]] = decodeURIComponent(field[1])
        if (i + 1 === fields.length) {
          resolve(data);
        }
      }
    });
  },

  encodeData: function(data, cb) {
    var stringified = JSON.stringify(data);
    var baseEncoded = btoa(stringified)
    // var encoded = encodeURIComponent(baseEncoded)
    return cb(baseEncoded);
  },

  decodeData: function(data, cb) {
    try {
      parsedData = JSON.parse(atob(data)) || null
    } catch (e) {
      // go to 404
      console.log('Error! Invalid data')
      return false
    }
    return cb(parsedData);
  }
}